const chai = require('chai');
const {expect} = require('chai');

const http = require('chai-http');
chai.use(http);

describe('api_test_suite', ()=> {
	it('test_api_get_people_is_running', ()=>{
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err,res)=>{
			expect(res).to.not.equal(undefined);
		})
	})
	it('test_api_get_people_returns_200', (done)=>{
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err,res)=>{
			expect(res.status).to.equal(200);
			done();
		})
	})
	it('test_api_post_person_returns_400_if_no_person_name', (done)=>{
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias:'James',
			age:28
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400);
			done();
		})
	})
	// it('test_api_post_person_returns_400_if_age_is_string', (done)=>{
	// 	chai.request('http://localhost:5001')
	// 	.post('/person')
	// 	.type('json')
	// 	.send({
	// 		name:'James',
	// 		age:"Twenty-Eight"
	// 	})
	// 	.end((err,res)=>{
	// 		expect(res.status).to.equal(400);
	// 		done();
	// 	})
	// })
	it('test_api_post_person_is_running', (done)=>{
		chai.request('http://localhost:5001')
		.post('/person')
		.end((err,res)=>{
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_person_returns_400_if_no_ALIAS', (done)=>{
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias:'',
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400);
			done();
		})
	})
	it('test_api_post_person_returns_400_if_no_AGE', (done)=>{
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			age:'',
		})
		.end((err,res)=>{
			expect(res.status).to.equal(400);
			done();
		})
	})


})

